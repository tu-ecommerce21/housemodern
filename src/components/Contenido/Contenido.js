import React  from 'react'
import ChatBot from 'react-simple-chatbot'
import { ThemeProvider } from 'styled-components'
import NameUser from '../NameUser/NameUser'
import WikipediaSearch from '../WikipediaSearch/WikipediaSearch'

const theme = {
    background: '#f5f8fb',
    headerBgColor: '#0cb3c9',
    headerFontColor: '#fff',
    headerFontSize: '20px',
    botBubbleColor: '#0cb3c9',
    botFontColor: '#fff',
    userBubbleColor: '#0cb3c9',
    userFontColor: '#fff',
}
 
function Contenido() {
        return (
            <>
            <section  id="contact" className="single-bg">
            <ThemeProvider theme={theme}>
                <ChatBot 
                    steps={[
                        {
                            id: "1",
                            message: "Hola soy Lucy, tu Agente Virtual, Favor indiqueme su nombre",
                            trigger: "2"
                        },
                        {
                            id: "2",
                            user: true,
                            validator: (value) => {
                                if (/^[A-Z]{0}[a-z]{2,15}$/.test(value)) {
                                    return true;
                                }
                                else {
                                    return 'Favor ingrese un nombre válido.';
                                }
                            },
                            trigger: "3"
                        },
                        {
                            id: "3",
                            // component: <NameUser />,
                            message: "Hola {previousValue}, ¡Te damos la Bienvenida!",
                            trigger: "4"
                        },
                        {
                            id: "4",
                            message: "Seleccione por favor la opción que necesita?",
                            trigger: "seleccion"
                        },
                        {
                            id: "seleccion",
                            options: [
                                {value: "f", label: "Construir tu propio proyecto", trigger: "7A"},
                                {value: "b", label: "Construir nuestros Modelos de Casas", trigger: "7B"},
                            ]
                        },
                        {
                            id: "Fin",
                            message: "Lo siento, si yo no puedo ayudarle. Le veo después",
                            end: true
                        },
                        {
                            id: "7A",
                            message: "Genial, la construcción es nuestro fuerte, que desear construir?",
                            trigger: "seleccionFront"
                        },
                        {
                            id: "7B",
                            message: "Son Hermosos nuestros modelos verdad? Ahora cual es el que mas te gusta?",
                            trigger: "seleccionBack"
                        },
                        {
                            id: "seleccionFront",
                            options: [
                                {label: "Quincho", value: "Quincho_(framework)", trigger: "9"},
                                {label: "Cierre y Portón", value: "Cierre y Portón_React", trigger: "9"},
                                {label: "Piscina", value: "Piscina_Vue.js", trigger: "9"},
                                {label: "Otro Proyecto", value: "Otro_Otro", trigger: "9"},
                            ]
                        },
                        {
                            id: "seleccionBack",
                            options: [
                                {label: "Casa Azucena 155 m2, 3 dormitorios 3 baños 1 piso", value: "Casa Azucena_Framework", trigger: "9"},
                                {label: "Casa Magnolia 135 m2, 3 dormitorios 2 baños 1 piso", value: "Casa Magnolia_Laravel", trigger: "9"},
                                {label: "Casa Tricahue 135 m2, 3 dormitorios 3 baños 1 piso", value: "Casa Tricahue_.Net Core", trigger: "9"},
                                {label: "Casa Queltehue 155 m2, 3 dormitorios 3 baños 1 piso", value: "Casa Queltehue_.Net 2", trigger: "9"},
                            ]
                        },  
                        {
                            id: "9",
                            component: <WikipediaSearch />,
                            asMessage: true,
                            trigger: "fin"
                           
                        },
                        {
                            id: "fin",
                            message: "El Agente Real te atenderá del otro lado por Whatsapp",
                            end: true
                        },
                        {
                            id: "preguntaVuelta",
                            message: "Necesitas alguna otra cosa?",
                            trigger: "respuestaVuelta",
                        }, 
                        {
                            id: "respuestaVuelta",
                            options: [
                                {value: "y", label: "Si", trigger: "4"},
                                {value: "n", label: "No", trigger: "Fin"},
                            ],
                        }

       
                       
                    ]}
                    />
                    </ThemeProvider>
             </section>   
       </>
    )
}

export default Contenido