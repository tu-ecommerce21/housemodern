import React from 'react'
// import {Link} from 'react-scroll'
function About2() {
    
    return (
        <>
            <section  id="about" className="single-bg ab2">
            <div className="section-block-title">
            <div className="bg-dark"></div>
                <div className="container">
                        <div className="section-sub-title center">
                            <article className="section-title-body white">
                                <h1 className="head-title">Un poco <span>Acerca</span> de nosotros</h1>
                            </article>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 order-lg-2 d-flex align-items-center">
                                <div className="widget-block relative">
                                <div className="h100 d-none d-lg-block"></div>
                                <h4 className="widget-title">El Origen de Casas Modernas</h4>
                                <p>
                                Mucho depende de tiempos, gustos y lo que anden buscando. En casas modernas nos hemos dado cuenta de lo caro que es comprarse una casa actualmente y lo chica y poco funcionales que pueden llegar a ser los departamentos de 50 m2. Por esa cantidad de dinero o inclusive menos es posible comprarse un terreno y construir una casa con nosotros a su gusto y preferencias 😎 . Vivir un poco más lejos de la cuidad, pero la tranquilidad lo vale. Ya no es excusa vivir lejos del centro para ir a trabajar, la tecnología nos permite hacer una gran cantidad de trabajo desde la casa.


                                </p>
                                    <div className="block-feature d-lg-none d-xl-block">
                                        <i className="ion-calendar"></i>
                                        <a href="/">Agenda con Nosotros</a>
                                        <p>
                                        La información de tus ideas queda protegida con nosotros, y te acompañaremos en hacerla realidad.
                                        </p>
                                    </div>
                                   
                                </div>
                            </div>
                            <div className="col-lg-6 order-lg-1 d-flex align-items-end">
                                <div  className="relative  d-flex">
                                        <div className="card-block  lax lax_preset_fadeIn:50:150  lax_preset_slideY:4500:700">
                                            <div className="card-info">
                                                <h2>100%</h2>
                                                <h4>Verificado</h4>
                                            </div>
                                        </div>
                                        <img className="img-fluid" src="img/preview/about3.png" alt=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
            {/* <div className="block color-scheme-1">
                <div className="container">
                    <div className="welcome-article-block">
                        <div className="row">
                            <div className="col-lg-4 col-md-12">
                                <div className="welcome-article dark y-50">
                                    <img className="img-fluid" alt="" src="img/preview/blog18.jpg"/>
                                    <div className="welcome-article-text">
                                        <h4>Fast Services1</h4>
                                        <p>Ut enim ad minim veniam, id anim id deserunt quis nostrud exercitation ullamco</p>
                                        <Link to='services' className="btn-default white btn-move" smooth={false} duration={800}>Visit Services</Link>
                            
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6">
                                <div className="welcome-article dark">
                                    <img className="img-fluid" alt="" src="img/preview/blog19.jpg"/>
                                    <div className="welcome-article-text">
                                        <h4>Creative Solution</h4>
                                        <p>Ut enim ad minim veniam, quis anim id deserunt nostrud exercitation ullamco</p>
                                        <Link to='portfolio' className="btn-default white btn-move t1" smooth={false} duration={800}>Visit Portfolio</Link>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6">
                                <div className="welcome-article dark y-50">
                                    <img className="img-fluid" alt="" src="img/preview/blog20.jpg"/>
                                    <div className="welcome-article-text">
                                        <h4>Last News</h4>
                                        <p>Ut enim ad minim veniam, quis id anim id  nostrud exercitation ullamco</p>

                                        <Link to='blog' className="btn-default white btn-move t2" smooth={false} duration={800}>Visit Blogs</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="h50 d-none d-lg-block"></div>
                </div>
            </div> */}

        </section>
        </>
    )
}

export default About2
