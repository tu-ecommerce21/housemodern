import React from 'react'
const services = [
    {
        _id:'001',
        classIcon:'ion-ios-pie-outline',
        title:'Construimos Modelos',
        text:'Escoga entre nuestros exclusivos modelos, si tienes otra idea en mente nosotros la diseñamos.'
    },
    {
        _id:'002',
        classIcon:'ion-key',
        title:'Velocidad',
        text:'Nuestro proceso es estandarizado, lo que nos permite ofrecer mejores tiempo de respuesta y una construcción más rápida'
    },
    {
        _id:'003',
        classIcon:'ion-ios-reverse-camera-outline',
        title:'Confianza',
        text:'Equipo conformado por constructores civiles con experiencia en construcción a lo largo de todo chile'
    },
    {
        _id:'004',
        classIcon:'ion-android-star',
        title:'Finas Terminaciones',
        text:'Todas nuestras casas cumplen con altos estandares de calidad y finas terminaciones, para hacer de tu hogar el lugar ideal para vivir.'
    } 
]
function Service3() {
    return (
        <>
        <section  id="services">
        <div className="bg-dark"></div> 
            <div className="section-block-title">
                <div className="section-title">   
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-md-8 col-sm-10">
                                <article className="section-title-body">
                                    <h1 className="head-title wow fadeInDown animated" data-wow-duration="1.5s">Nuestro Compromiso</h1>
                                    <div className="section-col wow fadeInUp animated" data-wow-duration="1.5s"></div>
                                    <p className="head-text wow fadeInUp animated" data-wow-duration="1.5s">
                                       Cada Proyecto en nuestras manos, nos comprometemos a:
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div> 
                </div>
                <div className="block color-scheme-blur">
                    <div className="container">
                        <div className="row">
                            {services.map((service)=>(
                            <div className="col-lg-4 col-md-6"  key={service._id}>
                                <div className="block-feature">
                                    <i className={service.classIcon}></i>
                                    <a href="/">{service.title}</a>
                                    <p>
                                    {service.text}
                                    </p>
                                </div>
                            </div>
                            ))}
                        </div>
                    </div>
                </div>
            
        </section>
        </>
    )
}

export default Service3
