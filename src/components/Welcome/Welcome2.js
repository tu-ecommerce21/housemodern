import React from 'react'

function Welcome2() {
    return (
        <section>
            <div className="block color-scheme-1">
                
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-10 col-lg-9">
                            <div className="section-sub-title center">
                                <article className="section-title-body white">
                                    <h1 className="head-title">
                                        <span>Casas Modernas</span> es tu espacio donde compartes con nosotros nuestros proyectos y si tu quieres también tus <span>ideas</span>.</h1>
                                    <p className="head-text">
                                        Cada uno de nuestros clientes logran llevarse con ellos la experiencia de crear sus sueños.
                                    </p>
                                </article>
                            </div>   
                        </div>
                    </div>
                </div>

            </div>
        </section>
    )
}

export default Welcome2
