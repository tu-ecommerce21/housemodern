import React, {useState} from 'react'
import {Link} from 'react-scroll'
import logo from './../../images/logo-big-shop.png'

function Navbar2() {
    const [navToggle, setnavToggle] = useState(true);

    return (
        <>
        <nav className="navbar navbar-default  navbar-expand-md">
                            <div className="navbar-header">
                                <Link to='home'  className="header-logo"  smooth={false} duration={800}>
                                    <span></span>
                                    <img src={logo} alt="logo"/>
                                </Link>
                                <button onClick={()=>setnavToggle(!navToggle)} type="button" className="navbar-toggle" data-bs-toggle="collapse" data-bs-target="#bs-example-navbar-collapse-1" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                                
                            </div>
                <div className={navToggle?'collapse navbar-collapse':'collapse navbar-collapse show'}  id="bs-example-navbar-collapse-1">
                    <ul className="nav navbar-nav  navbar-right menu-item-6" id="navbar-full">
                        <li className="current home"><span className="bg-icon"><i className="ion-ios-home-outline"></i></span><span className="rotated-title effect">Inicio</span><Link to='home' className="link-item " smooth={false} duration={800} activeClass="active">Inicio</Link></li>
                        <li className="about ab2"><span className="bg-icon"><i className="ion-ios-person-outline"></i></span><span className="rotated-title effect">Acerca</span><Link to='about' className="link-item" smooth={false} duration={800} activeClass="active">Acerca</Link></li>                                            
                        <li className="service"><span className="bg-icon"><i className="ion-ios-star-outline"></i></span><span className="rotated-title effect">Venta de Casas</span><Link to='services' className="link-item" smooth={false} duration={800} activeClass="active">Venta de Casas</Link></li>
                        <li className="work"><span className="bg-icon"><i className="ion-ios-photos-outline"></i></span><span className="rotated-title effect">Trabajos</span><Link to='portfolio' className="link-item" smooth={false} duration={800} activeClass="active">Trabajos</Link></li>
                        <li className="blog"><span className="bg-icon"><i className="ion-ios-chatboxes-outline"></i></span><span className="rotated-title effect">Testimonios</span><Link to='blog' className="link-item" smooth={false} duration={800} activeClass="active">Testimonios</Link></li> 
                        <li className="contact"><span className="bg-icon"><i className="ion-ios-telephone-outline"></i></span><span className="rotated-title effect">Comenzar</span><Link to='contact' className="link-item" smooth={false} duration={800} activeClass="active">Comenzar</Link></li> 
                    </ul>
                </div>
            </nav>
        </>
    )
}

export default Navbar2
