import React from 'react'
// import ContactForm from './../Contact/ContactForm'
import Contenido from './../Contenido/Contenido'

function Contact2() {
    return (
        <>
        <section  id="contact" className="single-bg">
            <div className="bg-dark"></div>
            <div className="block">
                
                    <div className="section-sub-title center">
                        <article className="section-title-body white">
                            <h1 className="head-title">Complete las preguntas y nos vemos del otro lado.</h1>
                        </article>
                    </div>
                    <div className="container">
                        <div className="row">
                           <div className="col-lg-6 ">
                                    
                                    <div className="text-center white">
                                     {/*  <h4 className="widget-title"><i className="ion-home"></i>Address:</h4>
                                        <p>
                                           
                                        </p>  */}
                                        <h4 className="widget-title"><i className="ion-star"></i>Ejecutivo:</h4>

                                        <p>Un Ejecutivo lo contactará en menos de 24 horas<br/> 
                                        </p>
                                        <h4 className="widget-title"><i className="ion-star"></i>Portafolio:</h4>
                                        <p>Al momento de comenzar, se armará su portafolio
                                        </p>  
                                    </div>

                            </div>  
                            <div className="col-lg-6">
                                   <Contenido /> 
                            </div>
                                    
                        </div>
                    </div>
            </div>
        </section>
        </>
    )
}

export default Contact2
