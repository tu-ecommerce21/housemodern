import React, { Component } from 'react';

class WikipediaSearch extends Component {
 
    constructor(props) {
        super(props);
        const { steps } = this.props;
        const { seleccion, seleccionFront, seleccionBack } = steps;
        this.state = {
            seleccion,
            seleccionFront,
            seleccionBack,
            busqueda: "",
            nombreCurado: "",
            urlwhatsapp: "",
            dear: "https://wa.me/56931313896?text=Hola%20Mónica%20Me%20atendió%20la%20agente%20virtual%20Lucy%20y%20me%20derivo%20con%20usted.Me%20interesa%20",
        }
    }

    componentDidMount() {
        if (this.state.seleccion.value === "f") {
            // Construccion
            this.setState({
                busqueda: this.state.seleccionFront.value,
            });
            if (this.state.seleccionFront.value.includes("_")) {
                var texto = this.state.seleccionFront.value;
                texto = texto.substring(0, texto.indexOf("_"));
                this.setState({
                    nombreCurado: texto,
                    urlwhatsapp: this.state.dear + "%20Contruir%20" + texto
                });
            } else {
                this.setState({
                    nombreCurado: this.state.seleccionFront.value,
                    urlwhatsapp: this.state.dear + "%20Contruir%20" +  this.state.nombreCurado
                });
            }
        } else if (this.state.seleccion.value === "b") {
            // Propiedades
            this.setState({
                busqueda: this.state.seleccionBack.value,
            });
            if (this.state.seleccionBack.value.includes("_")) {
                texto = this.state.seleccionBack.value;
                texto = texto.substring(0, texto.indexOf("_"));
                this.setState({
                    nombreCurado: texto,
                    urlwhatsapp: this.state.dear + "%20Ver%20o%20Comprar%20El%20Modelo%20" + texto
                });
            } else {
                this.setState({
                    nombreCurado: this.state.seleccionBack.value,
                    urlwhatsapp: this.state.dear + "%20Ver%20o%20Comprar%20El%20Modelo%20" +  this.state.nombreCurado
                });
            }
        } 
        
    }

    render() {
        return (

            
            <div>
              {/*   <p>Here's a link to the Wikipedia page of {this.state.nombreCurado}: </p> */}
                {/* <a href={"https://es.wikipedia.org/wiki/" + this.state.busqueda}  >{this.state.nombreCurado}</a> */}
                <a href={this.state.urlwhatsapp}>click aquí WhatsApp</a>
               
                

            </div>
        )
    }

    

}
export default WikipediaSearch;