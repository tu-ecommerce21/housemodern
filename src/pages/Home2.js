import React from 'react';
import Header2 from './../components/Header/Header2';
import Welcome2 from './../components/Welcome/Welcome2';
import About2 from './../components/About/About2';
import Service3 from './../components/Service/Service3';
import Portfolio1 from './../components/Portfolio/Portfolio1';
import Blogs2 from './../components/Blogs/Blogs2';
/* import Clients from './../components/Clients'; */
 import Contact2 from './../components/Contact/Contact2';
// import Newsletter2 from './../components/Newsletter/Newsletter2';
/* import Map from './../components/Map'; */
// import Footer2 from './../components/Footer/Footer2';
import SimpleSlider2 from './../components/Sliders/Slick-slider2'
//import bg_vide from './../videos/bigway.mp4'
//import bg_vide from './../videos/girl.mp4'
import bg_vide from './../videos/cinta.mp4' 
import './Home2.css';
function Home2() {
  return (
    <>
      <div  id="home" className="full-screen-block">
        <video autoPlay loop muted className="bg_video">
                <source src={bg_vide} type="video/mp4"></source>
        </video>
        
        
        <div className="bg-inner-light"></div>
        <Header2/>
        <SimpleSlider2/>
      </div>
      <Welcome2/>
      <About2/>
      <Service3/>
   {/*    <Newsletter2/> */}
      <Portfolio1/>
      <Blogs2/>
      {/* <Clients/>     */}
      <Contact2/> 
  {/*     <Map/> */}
     {/*  <Footer2/> */}
    </>
  );
}

export default Home2;